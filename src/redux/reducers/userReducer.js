const initialState = {
    value: [],
};

const taskReducer = (state = initialState, action) => {
    switch (action.type) {
        case "ADD":
            return {
                value: [...state.value, action.payload],
            };

        default:
            return state;
    }
};

export default taskReducer;
