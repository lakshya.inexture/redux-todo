import { createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import taskReducer from "./reducers/userReducer";
import rootReducer from "./reducers/userReducer";

const store = createStore(taskReducer, composeWithDevTools());

export default store;
