import { combineReducers } from "redux";
import taskReducer from "./reducers/userReducer";

const rootReducer = combineReducers(taskReducer);

export default rootReducer;
