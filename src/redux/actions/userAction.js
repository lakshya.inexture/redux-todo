export const addTask = (task) => {
    return {
        type: "ADD",
        payload: task,
    };
};
