import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";

function App() {
    const { value } = useSelector((state) => state);
    const dispatch = useDispatch();
    const [getTask, setGetTask] = useState("");
    const handleInput = (e) => {
        setGetTask(e.target.value);
    };
    const handleClick = () => {
        dispatch({ type: "ADD", payload: getTask });
        setGetTask("");
    };
    return (
        <div>
            <input type="text" value={getTask} onChange={handleInput} />
            <button onClick={handleClick}>Add</button>
            <ul>
                {value.map((e, i) => (
                    <li key={i}>{e}</li>
                ))}
            </ul>
        </div>
    );
}

export default App;
